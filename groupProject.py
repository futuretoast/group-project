#!/usr/bin/python3
# Line above lets us call program directly from a linux shell
# This is free and unencumbered software released into the public domain.
#
# ----------------------------------------------------------------------
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ----------------------------------------------------------------------
# Check if library files exist and stop program if not
from multiprocessing import Process
from pathlib import Path
from urllib import request
from time import sleep
from random import randint
constPath = Path("./graphics.py")
while constPath.is_file() == False:
    # Download library if not present
    print("Error: Graphics library not found. Attempting to download")
    try:
        request.urlretrieve("https://mcsp.wartburg.edu/zelle/python/graphics.py", "./graphics.py")
    except:
        print("Unknown error!")
constPath=Path("./gameLib.py")
if constPath.is_file():
    # Do nothing if library is found
    pass
else:
    print("Error: Game libs not found. Please place gameLib.py in the same folder.")
    exit()
# Import functions from gameLib
from gameLib import gameMenu, printRules, genCard, currentScore, replayChoice, writeToLog, gameDemo, renderCard, getCardIndex
# Check that gameLib works on current system
#libPreq()
# Show 3 example and then move to game menu
# Comment out demo runs for debug cycles
#for temp in range(5):
#    gameDemo()
# Deinitialize temp variable since we don't need it's contents anymore
#del temp
#sleep(1)
# Make sure low level graphics lib is present, since we do need it in the main file a bit
from graphics import GraphWin, Text, Rectangle, Point, Entry
winMain = GraphWin("Game v5", 640, 480)
temp = Text(Point(640/2,480/2),"Welcome to \"What's the card?\" A simple card game.")
temp.draw(winMain)
sleep(1)
temp.undraw()
del temp
tempText = Text(Point(640/2,(480/2)-40),"Care to tell me your name?: ")
tempText.draw(winMain)
nameBox = Entry(Point(320,240),20)
nameBox.draw(winMain)
print("nameBox drawn")
enterBox = Rectangle(Point(380, 315), Point(260, 275))
enterBox.setFill('green')
boxText = Text(Point(320,295), "Enter")
enterBox.draw(winMain)
boxText.draw(winMain)
print("Setting clickpoint")
clickPoint = winMain.getMouse()
print("clickPoint set")
while clickPoint.getX() >= 380 or clickPoint.getX() <= 260 or clickPoint.getY() >= 315 or clickPoint.getY() <= 275:
    #if clickPoint.getX() >= 380 or clickPoint.getX() <= 260 or clickPoint.getY() >= 315 or clickPoint.getY() <= 275:
        clickPoint = winMain.getMouse()
        print("Button not clicked")
#    else:
print("Button clicked")
clickPoint = winMain.getMouse()
boxText.undraw()
enterBox.undraw()
nameBox.undraw()
tempText.undraw()
# Display the actual menu
menuChoice = gameMenu(winMain)
while menuChoice != 4:
    gameMenu(winMain)
    # If user chooses option 1 print rules and reopen menu
    if menuChoice == 1:
        printRules()
        menuChoice = gameMenu(winMain)
    # If user chooses option 2 print rules and exit game
    elif menuChoice == 2:
        currentScoreValue = 0
        #TODO: Set this to 13 in production
        for runCount in range(13):
            # Run main game 5 times, clearing the screen before each run
            # Set card value
            cardValue = genCard()
            # Get user input for what they think the card is
            renderCard(cardValue,-1,0,winMain)
            sleep(1)
            cardIndexValue = randint(1,4)
            if cardIndexValue == 1:
                cardArrayValue1 = renderCard(cardValue,1,0,winMain)
                cardArrayValue2 = renderCard(genCard(),2,0,winMain)
                cardArrayValue3 = renderCard(genCard(),3,0,winMain)
                cardArrayValue4 = renderCard(genCard(),4,0,winMain)
                cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
            elif cardIndexValue == 2:
                cardArrayValue1 = renderCard(genCard(),1,0,winMain)
                cardArrayValue2 = renderCard(cardValue,2,0,winMain)
                cardArrayValue3 = renderCard(genCard(),3,0,winMain)
                cardArrayValue4 = renderCard(genCard(),4,0,winMain)
                cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
            elif cardIndexValue == 3:
                cardArrayValue1 = renderCard(genCard(),1,0,winMain)
                cardArrayValue2 = renderCard(genCard(),2,0,winMain)
                cardArrayValue3 = renderCard(cardValue,3,0,winMain)
                cardArrayValue4 = renderCard(genCard(),4,0,winMain)
                cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
            elif cardIndexValue == 4:
                cardArrayValue1 = renderCard(genCard(),1,0,winMain)
                cardArrayValue2 = renderCard(genCard(),2,0,winMain)
                cardArrayValue3 = renderCard(genCard(),3,0,winMain)
                cardArrayValue4 = renderCard(cardValue,4,0,winMain)
                cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
            userCard = False
            while userCard == False:
                userCard = getCardIndex(winMain)
            for i in range(4):
                renderCard(cardValue,cardIndexValue,1,winMain,cardBoxArray[i])
            # Check if card user entered is the actual
            # card value and ask them to play again if it is
            if cardIndexValue == userCard:
                # Print "Correct!" and display current score
                currentScoreValue = currentScore(True, currentScoreValue)
                print("Correct! Your current score is",currentScoreValue*10)
                sleep(1)
                logValue = 'runCount' + str(runCount) + '/' + str(currentScoreValue)
                writeToLog(logValue, "cards.txt")
            else:
                #  Print "Incorrect!" and display current score
                currentScoreValue = currentScore(False, currentScoreValue)
                print("Incorrect! Your current score is",currentScoreValue*10)
                sleep(1)
                logValue = 'runCount' + str(runCount) + '/' + str(currentScoreValue)
                writeToLog(logValue, "cards.txt")
        # Print final score
        finalScore = ("Your final score is " + str(currentScore("Final",currentScoreValue)))
        finalScoreText = Text(Point(320,80),finalScore)
        finalScoreText.draw(winMain)
        #finalScoreText.undraw()
        replayChoiceValue = replayChoice(winMain)
        #while replayChoiceValue != False:
        if replayChoiceValue == True:
            finalScoreText.undraw()
            menuChoice = gameMenu(winMain)
        else:
            replayChoiceValue = replayChoice()
        #if replayChoiceValue == False:
        #    print("I hope to see you again!")
        #    sleep(2)
        #    exit()
        #else:
        #    menuChoice = gameMenu(winMain)
    # If user chooses option 3 dynamically create a card scenario
    elif menuChoice == 3:
        from time import sleep
        gameDemo(winMain)
        menuChoice = gameMenu(winMain)
    elif menuChoice == False:
        menuChoice = gameMenu(winMain)
