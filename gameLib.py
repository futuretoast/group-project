# ----------------------------------------------------------------------
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
# ----------------------------------------------------------------------
# Print game intro and get user's name
# Work on simplifying button drawing
def clickButton(xVal,yVal,width,height,buttonText,winName,color="Green"):
    from graphics import Rectangle, Text, Point
    width=width/2
    height=height/2
    clickButton=Rectangle(Point(xVal+width,yVal+height),Point(xVal-width,yVal-height))
    clickText=Text(Point(xVal,yVal),buttonText)
    clickButton.setFill(color)
def writeToLog(dataToWrite,logFileName="log.log"):
    logFileName = open(logFileName, "a")
    dataToWrite = str(dataToWrite) + '\n'
    logFileName.write(dataToWrite)
    logFileName.close()
# Code for main game menu lib
def gameMenu(winName):
    from graphics import Rectangle, Point, Text
    menuBox1 = Rectangle(Point(220,10),Point(420,50))
    menuBox1.setFill("Green")
    textBox1 = Text(Point(320,30),"See rules")
    menuBox2 = Rectangle(Point(220,60),Point(420,100))
    menuBox2.setFill("Green")
    textBox2 = Text(Point(320,80),"Play Game")
    menuBox3= Rectangle(Point(220,110),Point(420,150))
    menuBox3.setFill("Green")
    textBox3 = Text(Point(320,130),"See an example")
    menuBox4 = Rectangle(Point(220,160),Point(420,200))
    menuBox4.setFill("Red")
    textBox4 = Text(Point(320,180),"Exit")
    menuBox1.draw(winName)
    textBox1.draw(winName)
    menuBox2.draw(winName)
    textBox2.draw(winName)
    menuBox3.draw(winName)
    textBox3.draw(winName)
    menuBox4.draw(winName)
    textBox4.draw(winName)
    clickPoint = winName.getMouse()
    if clickPoint.getX() >= 220 and clickPoint.getX() <= 420 and clickPoint.getY() <= 50 and clickPoint.getY() >= 10:
        print("Rules")
        menuBox1.undraw()
        textBox1.undraw()
        menuBox2.undraw()
        textBox2.undraw()
        menuBox3.undraw()
        textBox3.undraw()
        menuBox4.undraw()
        textBox4.undraw()
        return(1)
    elif clickPoint.getX() >= 220 and clickPoint.getX() <= 420 and clickPoint.getY() <= 100 and clickPoint.getY() >= 60:
        print("Start games")
        menuBox1.undraw()
        textBox1.undraw()
        menuBox2.undraw()
        textBox2.undraw()
        menuBox3.undraw()
        textBox3.undraw()
        menuBox4.undraw()
        textBox4.undraw()
        return(2)
    elif clickPoint.getX() >= 220 and clickPoint.getX() <= 420 and clickPoint.getY() <= 150 and clickPoint.getY() >= 110:
        print("Example")
        menuBox1.undraw()
        textBox1.undraw()
        menuBox2.undraw()
        textBox2.undraw()
        menuBox3.undraw()
        textBox3.undraw()
        menuBox4.undraw()
        textBox4.undraw()
        return(3)
    elif clickPoint.getX() >= 220 and clickPoint.getX() <= 420 and clickPoint.getY() <= 200 and clickPoint.getY() >= 160:
        print("Exit")
        menuBox1.undraw()
        textBox1.undraw()
        menuBox2.undraw()
        textBox2.undraw()
        menuBox3.undraw()
        textBox3.undraw()
        menuBox4.undraw()
        textBox4.undraw()
        return(4)
        exit()
    else:
        menuBox1.undraw()
        textBox1.undraw()
        menuBox2.undraw()
        textBox2.undraw()
        menuBox3.undraw()
        textBox3.undraw()
        menuBox4.undraw()
        textBox4.undraw()
        return(False)
def printRules():
    from graphics import GraphWin, Text, Point, Rectangle
    # Print actual rules
    winRules = GraphWin("Game v5 debug build", 640, 480)
    Text(Point(320,10),"The rules are simple.").draw(winRules)
    Text(Point(320,30),"I'll show 10 cards and then").draw(winRules)
    Text(Point(320,50),"ask what they are one at a ").draw(winRules)
    Text(Point(320,70),"time. If you select correctly").draw(winRules)
    Text(Point(320,90),"You get 10 points, but if you're").draw(winRules)
    Text(Point(320,110),"incorrect you lose 20").draw(winRules)
    temp=Rectangle(Point(220,250),Point(420,290))
    temp.setFill("Red")
    temp.draw(winRules)
    del temp
    Text(Point(320,270),"Close").draw(winRules)
    clickPoint=winRules.getMouse()
    while clickPoint.getX() <= 220 or clickPoint.getX() >= 420 or clickPoint.getY() <= 250 or clickPoint.getY() >=290:
        clickPoint = winRules.getMouse()
    winRules.close()
def printRulesExit():
    from time import sleep
    # Print actual rules
    print("Shame you're not playing")
    print("The rules were simple.")
    print("I'd show 5 cards and then")
    print("ask what they were. You'd just")
    print("type the answer to get points")
    print("I hope you come back to play")
    # Wait 3 seconds for user to read exit message
    sleep(3)
    # Clear screen and exit
    exit()

def genCard():
    from random import randint
    from time import sleep
    # Define suits in a list
    colorList = ["Red", "Green", "Blue", "Yellow"]
    # Set card number to a random number between 1 and 13
    numberValue = randint(1,13)
    # Pick a random suit from the list of suits
    colorValue = colorList[randint(0,3)]
    cardValue = [numberValue, colorValue]
    return(cardValue)


    # Clear screen, generate card,
    # then show card on screen
    genCard()
    print(cardValue)
    # Wait 1 second before clearing screen
    # to give the user a chance to win
    sleep(1)

# Actually tracks number of wins/loses despite the name
def currentScore(didUserWin, winCount = 0):
    winCount = int(winCount)
    if didUserWin == True:
        winCount = winCount + 1
        return(winCount)
    elif didUserWin == False:
        winCount = winCount - 2
        return(winCount)
    elif didUserWin == "Final":
        winCount = winCount * 10
        return(winCount)
    else:
        # Print error if function does not execute correctly
        print("Unknown error")
        print("Technical info: Unknown error in currentScore function")
    winCount = winCount * 10
    return(winCount)
# Lib for asking if user wants to play again
def replayChoice(winName):
    from graphics import Rectangle, Text, Point
    from time import sleep
    text = Text(Point(320,100),"Do you want to play again?")
    text.draw(winName)
    yesBox = Rectangle(Point(260,120),Point(380,190))
    yesBox.setFill("Green")
    yesText = Text(Point(320,155),"Yes")
    noBox = Rectangle(Point(260,200),Point(380,270))
    noBox.setFill("Red")
    noText = Text(Point(320,235),"No")
    yesBox.draw(winName)
    yesText.draw(winName)
    noBox.draw(winName)
    noText.draw(winName)
    userChoice = None
    clickPoint = winName.getMouse()
    while userChoice == None:
        if clickPoint.getX() >= 260 and clickPoint.getX() <= 380 and clickPoint.getY() >= 120 and clickPoint.getY() <= 190:
            userChoice = True
        elif clickPoint.getX() >= 260 and clickPoint.getX() <= 380 and clickPoint.getY() >= 200 and clickPoint.getY() <= 270:
            userChoice = False
        else:
            clickPoint = winName.getMouse()
    text.undraw()
    yesBox.undraw()
    yesText.undraw()
    noBox.undraw()
    noText.undraw()
    return(userChoice)
def gameDemo(winName):
    from random import randint
    # Randomly choose to win or loose
    #demoChoice = randint(1,2)
    #if demoChoice == 1:
    #    demoWin(winName)
    #elif demoChoice == 2:
    #    demoLose()
    #demoReplayChoice()
    demoWin(winName)
def demoLose():
    from time import sleep
    print("This is an example game-play")
    print("This will show you what to expect")
    # Set card value
    cardValue = genCard()
    # Print card value, wait and clear screen
    print(cardValue)
    sleep(1)
    # Print card prompt without newline
    print("Please enter the card shown: ",end='')
    # Generate a card for the simulated user to enter
    tempCardValue = genCard()
    if tempCardValue != cardValue:
        # Directly print card value if not equal
        print(tempCardValue)
    elif tempCardValue == cardValue and tempCardValue == "2h":
        # Check if static value and print value if not
        print("4d")
    else:
        # If generated card equals static value
        # print different static value
        print("2h")
    sleep(2)
    # Print that simulated user lost 20 points.
    print("Your score is -20")
    sleep(1)
def demoReplayChoice():
    from time import sleep
    # Simulate user choosing to replay game
    print("Want to play again?")
    print("[y/n] ",end='')
    sleep(1)
    print("y")
    sleep(1)
def getCardIndex(winName):
    clickPoint = winName.getMouse()
    if clickPoint.getX() >= 48 and clickPoint.getX() <= 148 and clickPoint.getY() >= 140 and clickPoint.getY() <= 340:
        return(1)
    elif clickPoint.getX() >= 196 and clickPoint.getX() <= 296 and clickPoint.getY() >= 140 and clickPoint.getY() <= 340:
        return(2)
    elif clickPoint.getX() >= 344 and clickPoint.getX() <= 444 and clickPoint.getY() >= 140 and clickPoint.getY() <= 340:
        return(3)
    elif clickPoint.getX() >= 492 and clickPoint.getX() <= 592 and clickPoint.getY() >= 140 and clickPoint.getY() <= 340:
        return(4)
    else:
        return(False)
def renderCard(cardVal=None,cardIndex=None,isCardFinal=0,winName=None,rawValue=None):
    # isCardFinal is poorly named. If isCardFinal is 0 we draw the card.
    # if isCardFinal is 1 we undraw it.
    # if isCardFinal is 2 we draw i being clicked
    from graphics import GraphWin, Rectangle, Point, Text
    from time import sleep
    if cardIndex == 1 and isCardFinal == 0:
        card = Rectangle(Point(48,140),Point(148,340))
        card.setFill(cardVal[1])
        text = Text(Point(98,240),cardVal[0])
        card.draw(winName)
        text.draw(winName)
        return([card, text, cardIndex])
    elif cardIndex == 2 and isCardFinal == 0:
        card = Rectangle(Point(196,140),Point(296,340))
        card.setFill(cardVal[1])
        text = Text(Point(246,240),cardVal[0])
        card.draw(winName)
        text.draw(winName)
        return([card, text, cardIndex])
    elif cardIndex == 3 and isCardFinal == 0:
        card = Rectangle(Point(344,140),Point(444,340))
        card.setFill(cardVal[1])
        text = Text(Point(394,240),cardVal[0])
        card.draw(winName)
        text.draw(winName)
        return([card, text, cardIndex])
    elif cardIndex == 4 and isCardFinal == 0:
        card = Rectangle(Point(492,140),Point(592,340))
        card.setFill(cardVal[1])
        text = Text(Point(542,240),cardVal[0])
        card.draw(winName)
        text.draw(winName)
        return([card, text, cardIndex])
    elif cardIndex == -1 and isCardFinal == 0:
        card =  Rectangle(Point(270,140),Point(370,340))
        card.setFill(cardVal[1])
        card.draw(winName)
        text = Text(Point(320,240),cardVal[0])
        text.draw(winName)
        sleep(1)
        card.undraw()
        text.undraw()
    elif isCardFinal == 0 and cardIndex != -1:
        card.draw(winName)
        text.draw(winName)
    elif isCardFinal == 1:
        print("Undrawing")
        rawValue[0].undraw()
        rawValue[1].undraw()
    elif isCardFinal == 2:
        rawValue[0].undraw()
        rawValue[1].undraw()
        if cardIndex == 1:
            card = Rectangle(Point(58,150),Point(138,330))
            card.setFill(cardVal[1])
            text = Text(Point(98,240),cardVal[0])
        elif cardIndex == 2:
            card = Rectangle(Point(206,150),Point(286,330))
            card.setFill(cardVal[1])
            text = Text(Point(246,240),cardVal[0])
        elif cardIndex == 3:
            card = Rectangle(Point(354,150),Point(434,330))
            card.setFill(cardVal[1])
            text = Text(Point(394,240),cardVal[0])
        elif cardIndex == 4:
            card = Rectangle(Point(502,150),Point(582,330))
            card.setFill(cardVal[1])
            text = Text(Point(542,240),cardVal[0])
        rawValue[0].undraw()
        rawValue[1].undraw()
        card.draw(winName)
        text.draw(winName)
        print("example drawn")
        sleep(1)
        card.undraw()
        text.undraw()
        rawValue[0].draw(winName)
        rawValue[1].draw(winName)
        print("Original drawn")
        sleep(1)
        rawValue[0].undraw()
        rawValue[1].undraw()
# Function for a point gain example
def demoWin(winName):
    from time import sleep
    from random import randint
    cardValue = genCard()
    renderCard(cardValue,-1,0,winName)
    sleep(1)
    cardIndexValue = randint(1,4)
    if cardIndexValue == 1:
        cardArrayValue1 = renderCard(cardValue,1,0,winName)
        cardArrayValue2 = renderCard(genCard(),2,0,winName)
        cardArrayValue3 = renderCard(genCard(),3,0,winName)
        cardArrayValue4 = renderCard(genCard(),4,0,winName)
        cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
    elif cardIndexValue == 2:
        cardArrayValue1 = renderCard(genCard(),1,0,winName)
        cardArrayValue2 = renderCard(cardValue,2,0,winName)
        cardArrayValue3 = renderCard(genCard(),3,0,winName)
        cardArrayValue4 = renderCard(genCard(),4,0,winName)
        cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
    elif cardIndexValue == 3:
        cardArrayValue1 = renderCard(genCard(),1,0,winName)
        cardArrayValue2 = renderCard(genCard(),2,0,winName)
        cardArrayValue3 = renderCard(cardValue,3,0,winName)
        cardArrayValue4 = renderCard(genCard(),4,0,winName)
        cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
    elif cardIndexValue == 4:
        cardArrayValue1 = renderCard(genCard(),1,0,winName)
        cardArrayValue2 = renderCard(genCard(),2,0,winName)
        cardArrayValue3 = renderCard(genCard(),3,0,winName)
        cardArrayValue4 = renderCard(cardValue,4,0,winName)
    sleep(1)
    cardBoxArray = [cardArrayValue1, cardArrayValue2, cardArrayValue3, cardArrayValue4]
    renderCard(cardValue,cardIndexValue,2,winName,cardBoxArray[cardIndexValue-1])
    for i in range(4):
        renderCard(cardValue,cardIndexValue,1,winName,cardBoxArray[i])
